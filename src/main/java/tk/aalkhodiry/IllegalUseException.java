package tk.aalkhodiry;


/**
 * Used to handel errors
 * @author Abdulrhman Alkhodiry <aalkhodiry@elm.sa>
 *
 */
public class IllegalUseException extends Exception {

	private static final long serialVersionUID = -7543104951550834738L;

	public IllegalUseException() {
		super();
	}
	
	public IllegalUseException(String message) {
		super(message);
	}
	
	public IllegalUseException(String message, Throwable cause) {
		super(message, cause);
	}
	
	public IllegalUseException(Throwable cause) {
		super(cause);
	}
}
