package tk.aalkhodiry;


/**
 * A unit of time for measuring a period, such as 'Days' or 'Minutes'. in milliSec
 * @author Abdulrhman Alkhodiry <aalkhodiry@elm.sa>
 */
public enum TimeUnit {
	WEEK(7 * 24 * 60 * 60 * 1000L), DAY(24 * 60 * 60 * 1000L), HOUR(60 * 60 * 1000L), MINUTE(60 * 1000L);

	private long UnitMilliSec;

	private TimeUnit(long value) {
		this.UnitMilliSec = value;
	}

	public long getMilliSec() {
		return this.UnitMilliSec;
	}
}
