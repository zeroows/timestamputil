package tk.aalkhodiry;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 * Timestamp util
 * 
 * @author Abdulrhman Alkhodiry <aalkhodiry@elm.sa>
 */
public class Timestamp {

	/**
	 * Java (1000 ms)
	 */
	private static long javeTimeAdd = 1000L;

	private Timestamp() {
	}

	private Timestamp(TimestampBuilder builder) {
	}

	/**
	 * returns the timestamp for now
	 * 
	 * @return
	 */
	public static synchronized long getTimestamp() {
		return (long) (System.currentTimeMillis() / javeTimeAdd);
	}

	/**
	 * returns the timestamp from the giving date
	 * 
	 * @param date
	 * @return
	 */
	public static synchronized long getTimestamp(java.util.Date date) {
		return (long) (date.getTime() / javeTimeAdd);
	}

	/**
	 * to get Timestamp for giving date and format
	 * <table border="1">
	 * <tr>
	 * <th>Code</th>
	 * <th>Meaning</th>
	 * </tr>
	 * <tr>
	 * <td>d</td>
	 * <td>(day): 2d or fewer (in text xx)</td>
	 * </tr>
	 * <tr>
	 * <td>y</td>
	 * <td>(year): y or fewer (one digit), >4y (full year)</td>
	 * </tr>
	 * <tr>
	 * <td>E</td>
	 * <td>(day of week): 3E or fewer (in text xxx), >3E (in full text)</td>
	 * </tr>
	 * <tr>
	 * <td>M</td>
	 * <td>(month): M (in number), MM (in number with leading zero) 3M: (in text
	 * xxx), >3M: (in full text full)</td>
	 * </tr>
	 * <tr>
	 * <td>h</td>
	 * <td>(hour): h, hh (with leading zero)</td>
	 * </tr>
	 * <tr>
	 * <td>m</td>
	 * <td>(minute)</td>
	 * </tr>
	 * <tr>
	 * <td>s</td>
	 * <td>(second)</td>
	 * </tr>
	 * <tr>
	 * <td>a</td>
	 * <td>(AM/PM)</td>
	 * </tr>
	 * <tr>
	 * <td>H</td>
	 * <td>(hour in 0 to 23)</td>
	 * </tr>
	 * <tr>
	 * <td>z</td>
	 * <td>(time zone)</td>
	 * </tr>
	 * </table>
	 * 
	 * exp: getTimestamp("E, y-M-d 'at' h:m:s a z",
	 * "Sat, 10-9-25 at 9:27:1 PM GMT")
	 * 
	 * @param format
	 * @param date
	 * @return
	 * @throws IllegalUseException
	 */
	public static long getTimestamp(String format, String date)
			throws IllegalUseException {
		try {
			Object obj = new Object();
			synchronized (obj) {
				return (long) (new SimpleDateFormat(format).parse(date)
						.getTime() / javeTimeAdd);
			}
		} catch (ParseException e) {
			throw new IllegalUseException(e.getMessage(), e);
		}
	}

	public synchronized static long getTimestamp(int year, int mon, int day)
			throws IllegalUseException {
		try {
			Object obj = new Object();
			synchronized (obj) {
				return (long) (new SimpleDateFormat("yyyy-MM-dd hh:mm:ss zzz")
						.parse(String.format("%d-%d-%d 00:00:00 GMT", year,
								mon, day)).getTime() / javeTimeAdd);
			}
		} catch (ParseException e) {
			throw new IllegalUseException(e.getMessage(), e);
		}
	}

	/**
	 * Builder Class
	 * 
	 * @author Abdulrhman Alkhodiry <aalkhodiry@elm.sa>
	 */
	public static class TimestampBuilder {
		private static long javeTimeAdd = 1000L;
		private java.util.Calendar calendar;

		public TimestampBuilder() {
			this.calendar = new java.util.GregorianCalendar(
					java.util.TimeZone.getTimeZone("GMT"));
		}

		public TimestampBuilder(java.util.Calendar calendar) {
			this.calendar = calendar;
		}

		public TimestampBuilder addMinutes(int mins) {
			this.calendar.add(Calendar.MINUTE, mins);
			return this;
		}

		public TimestampBuilder addHours(int hours) {
			this.calendar.add(Calendar.HOUR, hours);
			return this;
		}

		public TimestampBuilder addDays(int days) {
			this.calendar.add(Calendar.DATE, days);
			return this;
		}

		public TimestampBuilder addMonths(int months) {
			this.calendar.add(Calendar.MONTH, months);
			return this;
		}

		public long getTimestamp() {
			return (long) (this.calendar.getTime().getTime() / javeTimeAdd);
		}

	}
}
