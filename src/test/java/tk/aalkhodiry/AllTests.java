package tk.aalkhodiry;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ TimestampTest.class, TimeUnitTest.class})
public class AllTests {

}
