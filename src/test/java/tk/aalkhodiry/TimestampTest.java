package tk.aalkhodiry;


/**
 * Unit test for simple App.
 */
public class TimestampTest {
	/**
	 * Create the test case
	 * 
	 * @param testName
	 *            name of the test case
	 */

	public TimestampTest() {
	}

	/**
	 * Rigourous Test :-)
	 */
	@org.junit.Test
	public void testApp() {
		org.junit.Assert.assertTrue(true);
	}

	/**
	 * TimestampBuilder Test :-)
	 * 
	 */
	@org.junit.Test
	public void testBuilder() throws Exception {
		long timestamp = new Timestamp.TimestampBuilder().addDays(1).getTimestamp();
		org.junit.Assert.assertEquals(timestamp, (new java.util.Date().getTime() / 1000) + (24 * 60 * 60));
	}
	
	/**
	 * Date Test :-)
	 * @throws IllegalUseException 
	 * 
	 * @throws Exception
	 */
	@org.junit.Test
	public void testDate() throws IllegalUseException {
		long time = tk.aalkhodiry.Timestamp.getTimestamp(2013, 9, 15);
		org.junit.Assert.assertEquals(time, (1379203200L));
	}
}
